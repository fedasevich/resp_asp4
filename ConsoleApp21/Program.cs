﻿using System;
using System.Reflection;

public class Restaurant
{
    public string Name;
    private string Location;
    internal double Rating;
    public static string Cuisine;
    public int Tables { get; set; }

    public Restaurant(string name, string location, int tables, double rating, string cuisine)
    {
        Name = name;
        Location = location;
        Tables = tables;
        Rating = rating;
        Cuisine = cuisine;
    }

    public void ServeDish(string dishName)
    {
        Console.WriteLine($"Serving {dishName} of {Cuisine} cuisine to customers at {Location}.");
    }

    private void CalculateProfit(int customers, double averageBill)
    {
        double profit = customers * averageBill;
        Console.WriteLine($"Estimated profit: {profit}");
        Console.WriteLine($"Rating of the restaurant is {Rating} based on the profit of {profit}");
    }

    public void CloseRestaurant()
    {
        Console.WriteLine($"{Name} at {Location} with {Tables} tables is now closed.");
    }
}

class Program
{
    static void Main(string[] args)
    {
        Restaurant italianRestaurant = new Restaurant("My Restaurant", "New York", 10, 4.5, "Italian");

        Type type = italianRestaurant.GetType();
        Console.WriteLine($"Type is: {type.Name}");
        Console.WriteLine($"Type Namespace: {type.Namespace}");
        Console.WriteLine($"Type Full Name: {type.FullName}");

        TypeInfo typeInfo = type.GetTypeInfo();
        foreach (var prop in typeInfo.DeclaredProperties)
        {
            Console.WriteLine(prop.Name);
        }

        foreach (var method in typeInfo.DeclaredMethods)
        {
            Console.WriteLine(method.Name);
        }


        MemberInfo[] members = type.GetMembers();
        foreach (MemberInfo member in members)
        {
            Console.WriteLine($"Name: {member.Name}, Type: {member.MemberType}");
        }

        FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        foreach (var field in fields)
        {
                Console.WriteLine($"Internal Field: {field.Name} = {field.GetValue(italianRestaurant)}");

        }

        MethodInfo serveDishMethod = type.GetMethod("ServeDish");
        if (serveDishMethod != null)
        {
            serveDishMethod.Invoke(italianRestaurant, new object[] { "Pasta" });
        }

        MethodInfo calculateProfitMethod = type.GetMethod("CalculateProfit", BindingFlags.NonPublic | BindingFlags.Instance);
        if (calculateProfitMethod != null)
        {
            calculateProfitMethod.Invoke(italianRestaurant, new object[] { 100, 20.0 });
        }

        MethodInfo closeRestaurantMethod = type.GetMethod("CloseRestaurant");
        if (closeRestaurantMethod != null)
        {
            closeRestaurantMethod.Invoke(italianRestaurant, null);
        }
    }
}



